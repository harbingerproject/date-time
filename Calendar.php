<?php

/**
 * This file is part of Harbinger Project.
 *
 * Copyright (c) 2015, Gabriel Heming <gabriel.heming@hotmail.com>.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *
 *   * Neither the name of Gabriel Heming nor the names of his
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * @author Gabriel Heming <gabriel.heming@hotmail.com>
 * @copyright 2015 Gabriel Heming. All rights reserved.
 * @license http://www.opensource.org/licenses/bsd-license.php BSD License
 **/

namespace Harbinger\DateTime;

use Harbinger\Iterator\Filter;
use Harbinger\StandardLibrary\DateInterval;

/**
 * Represents a calendar
 * @package Harbinger
 * @subpackage DateTime
 * @author Gabriel Heming <gabriel.heming@hotmail.com>
 **/
class Calendar
{

    /**
     * @var \Harbinger\DateTime\Collection\Calendar\Recess
     **/
    private $recessCollection;

    /**
     * @param \Harbinger\DateTime\Collection\Calendar\Recess $recessCollection
     **/
    public function __construct(Collection\Calendar\Recess $recessCollection = null)
    {
        if (is_null($recessCollection)) {
            $recessCollection = new Collection\Calendar\Recess();
        }

        $this->setRecessCollection($recessCollection);
    }

    /**
     * Set the recess period collection
     * @param \Harbinger\DateTime\Collection\Calendar\Recess $recessCollection
     **/
    public function setRecessCollection(Collection\Calendar\Recess $recessCollection)
    {
        $this->recessCollection = $recessCollection;
    }

    /**
     * Retrieve the recess period collection
     * @return \Harbinger\DateTime\Collection\WorkPeriod
     **/
    public function getRecessCollection()
    {
        return $this->recessCollection;
    }

    /**
     * Check if the given date is a recess
     * @return \DateTimeInterface
     * @see http://php.net/manual/en/class.dateinterval.php DateInterval class documentation
     * @see http://php.net/manual/en/book.datetime.php DateTime library documentation
     **/
    public function isRecess(\DateTimeInterface $dateTime)
    {
        $callback = function ($recess) use ($dateTime) {
            return $recess->isRecess($dateTime);
        };

        foreach (new \CallbackFilterIterator($this->getRecessCollection() , $callback) as $recess) {
            return true;
        }

        return false;
    }
}
