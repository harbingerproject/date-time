<?php

/**
 * This file is part of Harbinger Project.
 *
 * Copyright (c) 2015, Gabriel Heming <gabriel.heming@hotmail.com>.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *
 *   * Neither the name of Gabriel Heming nor the names of his
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * @author Gabriel Heming <gabriel.heming@hotmail.com>
 * @copyright 2015 Gabriel Heming. All rights reserved.
 * @license http://www.opensource.org/licenses/bsd-license.php BSD License
 **/

namespace Harbinger\DateTime;

/**
 * Represents a worked time period
 * @package Harbinger
 * @subpackage DateTime
 * @author Gabriel Heming <gabriel.heming@hotmail.com>
 **/
class WorkedPeriod
{

    /**
     * @var \Harbinger\DateTime\Collection\Period
     **/
    private $periodCollection;

    /**
     * construct the worked period
     * @param \Harbinger\DateTime\Collection\Period $periodCollection
     **/
    public function __construct(Collection\Period $periodCollection = null)
    {
        $periodCollection = $periodCollection instanceof Collection\Period
                            ? $periodCollection
                            : new Collection\Period();

        $this->setPeriodCollection($periodCollection);
    }

    /**
     * Set the collection of periods
     * @param \Harbinger\DateTime\Collection\Period
     * @see http://php.net/manual/en/class.datetime.php DateTime class documentation
     * @see http://php.net/manual/en/book.datetime.php DateTime library documentation
     **/
    public function setPeriodCollection(Collection\Period $periodCollection)
    {
        $this->periodCollection = $periodCollection;
    }

    /**
     * Get the collection of periods
     * @return \Harbinger\DateTime\Collection\Period
     * @see http://php.net/manual/en/class.datetime.php DateTime class documentation
     * @see http://php.net/manual/en/book.datetime.php DateTime library documentation
     **/
    public function getPeriodCollection()
    {
        return $this->periodCollection;
    }


    public function __clone()
    {
        $periodCollection = new Collection\Period();

        foreach ($this->getPeriodCollection() as $period) {
            $periodCollection->add(clone $period);
        }

        $this->getPeriodCollection()->rewind();

        $this->setPeriodCollection($periodCollection);
    }
}
