<?php

/**
 * This file is part of Harbinger Project.
 *
 * Copyright (c) 2015, Gabriel Heming <gabriel.heming@hotmail.com>.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *
 *   * Neither the name of Gabriel Heming nor the names of his
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * @author Gabriel Heming <gabriel.heming@hotmail.com>
 * @copyright 2015 Gabriel Heming. All rights reserved.
 * @license http://www.opensource.org/licenses/bsd-license.php BSD License
 **/

namespace Harbinger\DateTime;

use Harbinger\Iterator\Filter;
use Harbinger\StandardLibrary\DateInterval;

/**
 * Represents a work period
 * @package Harbinger
 * @subpackage DateTime
 * @author Gabriel Heming <gabriel.heming@hotmail.com>
 **/
class WorkPeriod
{

    /**
     * @var \Harbinger\DateTime\Collection\Calendar
     * @see \Harbinger\Object\Collection
     **/
    private $calendarCollection;

    /**
     * @var \Harbinger\DateTime\Collection\Expedient
     * @see \Harbinger\Object\Collection
     **/
    private $expedientCollection;

    /**
     * @param \Harbinger\DateTime\Collection\Expedient $expedientCollection
     * @param \Harbinger\DateTime\Collection\Calendar $calendarCollection
     **/
    public function __construct(
        Collection\Expedient $expedientCollection = null,
        Collection\Calendar $calendarCollection = null
    ) {
        if (is_null($expedientCollection)) {
            $expedientCollection = new Collection\Expedient();
        }

        if (is_null($calendarCollection)) {
            $calendarCollection = new Collection\Calendar();
        }

        $this->setCalendarCollection($calendarCollection);
        $this->setExpedientCollection($expedientCollection);
    }

    /**
     * Define the calendar collection
     * @param \Harbinger\DateTime\Collection\Calendar $calendarCollection
     **/
    public function setCalendarCollection(Collection\Calendar $calendarCollection)
    {
        $this->calendarCollection = $calendarCollection;
    }

    /**
     * Define the period collection
     * @param \Harbinger\DateTime\Collection\Expedient $expedientCollection
     **/
    public function setExpedientCollection(Collection\Expedient $expedientCollection)
    {
        $this->expedientCollection = $expedientCollection;
    }

    /**
     * Retrieve the calendar collection
     * @return \Harbinger\DateTime\Collection\Calendar
     **/
    public function getCalendarCollection()
    {
        return $this->calendarCollection;
    }

    /**
     * Retrieve the period collection
     * @return \Harbinger\DateTime\Collection\Period
     **/
    public function getExpedientCollection()
    {
        return $this->expedientCollection;
    }

    /**
     * Check if the date/hour is between work period
     * @param DateTimeInterface $dateTime
     * @return boolean
     **/
    public function isBetween(\DateTimeInterface $dateTime)
    {
        if (!$this->isExpedientDay($dateTime)) {
            return false;
        }

        $callback = function ($expedient) use ($dateTime) {
            return $expedient->getPeriod()->isBetween($dateTime);
        };

        foreach (new \CallbackFilterIterator($this->getExpedientCollection() , $callback) as $expedient) {
            return true;
        }

        return false;
    }

    /**
     * Check if the date is an expedient day
     * @param \DateTimeInterface $dateTime
     * @return boolean
     **/
    public function isExpedientDay(\DateTimeInterface $dateTime)
    {
        if ($this->isRecess($dateTime)) {
            return false;
        }

        $callback = function ($expedient) use ($dateTime) {
            return $expedient->isExpedientDay($dateTime);
        };

        foreach (new \CallbackFilterIterator($this->getExpedientCollection() , $callback) as $expedient) {
            return true;
        }

        return false;
    }

    /**
     * Check if the date is a recess
     * @param \DateTimeInterface $dateTime
     * @return boolean
     **/
    public function isRecess(\DateTimeInterface $dateTime)
    {
        $callback = function ($calendar) use ($dateTime) {
            return $calendar->isRecess($dateTime);
        };

        foreach (new \CallbackFilterIterator($this->getCalendarCollection() , $callback) as $calendar) {
            return true;
        }

        return false;
    }

    /**
     * Calculate the time worked at work
     * @param Harbinger\DateTime\WorkedPeriod $workedPeriod
     * @return DateInterval
     **/
    public function calculateWorkedPeriod(WorkedPeriod $workedPeriod)
    {
        $periodCollection = $workedPeriod->getPeriodCollection();

        $dateInterval = new \DateInterval('PT0S');

        foreach ($periodCollection as $period) {
            if ($period->getStart() == $period->getEnd()) {
                $dateInterval = DateInterval::sum($dateInterval , $period->getEstimated());

                continue;
            }

            if ($this->isExpedientDay($period->getStart())) {
                $dateInterval = DateInterval::sum($dateInterval , $this->calculateWorkedPeriodWhentIsWorkDay($period));

                continue;
            }


            $dateInterval = DateInterval::sum($dateInterval , $this->calculateWorkedPeriodWhentIsNotWorkDay($period));
        }

        return $dateInterval;

    }

    private function calculateWorkedPeriodWhentIsWorkDay(Period $period)
    {
        try {
            return $this->calculateWorkedPeriodBetweenPeriodList($this->getExpedientCollection() , $period);
        } catch (RuntimeException $exception) {
            return new \DateInterval('PT0S');
        }
    }

    private function calculateWorkedPeriodBetweenPeriodList(Collection\Expedient $expedientList , Period $period)
    {
        $callback = function (Expedient $expedient) use ($period) {
            return $expedient->getPeriod()->isBetween($period->getStart());
        };

        foreach (new \CallbackFilterIterator($expedientList , $callback) as $expedient) {
            return $this->calculateWorkedPeriodBetweenWorkTime($expedient , $period);
        }

        $callback = function (Expedient $expedient) use ($period) {
            $isExpedientDay = $expedient->isExpedientDay($period->getStart());

            $isStartPeriodLesserThanWorkPeriod = (
                $this->normalizePeriodStartWork(
                    $expedient->getPeriod(),
                    $period
                ) >= $period->getStart()
            );

            return ($isExpedientDay && $isStartPeriodLesserThanWorkPeriod);
        };

        foreach (new \CallbackFilterIterator($expedientList , $callback) as $expedient) {
            $dateStartWork = $this->normalizePeriodStartWork($expedient->getPeriod() , $period);

            if (!isset($dateTimeStartWork)) {
                $dateTimeStartWork = $dateStartWork;
            }

            if ($dateStartWork < $dateTimeStartWork) {
                $dateTimeStartWork = $dateStartWork;
            }
        }

        if (isset($dateTimeStartWork)) {
            return $this->calculateWorkedPeriodWithNewWorkedPeriod($dateTimeStartWork , $period->getEnd());
        }

        return $this->calculateWorkedPeriodFromNextDay($period);
    }

    private function calculateWorkedPeriodWhentIsNotWorkDay(Period $period)
    {
        return $this->calculateWorkedPeriodWithNewWorkedPeriod(
            $this->goToNextDay($period->getStart()),
            $period->getEnd()
        );
    }

    /**
     * DateTime need be normalized cause he carry the default date with himself
     * @param Harbinger\DateTime\Period $period
     * @param Harbinger\DateTime\WorkedPeriod $workedPeriod
     * @return \DateTime
     **/
    private function normalizePeriodStartWork(Period $period , Period $workedPeriod)
    {
        $workedPeriodStart = $workedPeriod->getStart();

        $newDateTime = clone $period->getStart();
        $newDateTime->setDate(
            $workedPeriodStart->format('Y'),
            $workedPeriodStart->format('m'),
            $workedPeriodStart->format('d')
        );

        return $newDateTime;
    }

    /**
     * @param Harbinger\DateTime\Expedient $period
     * @param Harbinger\DateTime\Period $period
     * @return DateInterval
     **/
    private function calculateWorkedPeriodBetweenWorkTime(Expedient $expedient , Period $period)
    {
        $dateInterval = $expedient->calculateWorkedPeriod($period);

        $date1 = clone $date2 = new \DateTime();
        $date1->add($period->getEstimated());//Estimated date interval
        $date2->add($dateInterval);

        if ($date1 <= $date2) {
            return $dateInterval;
        }
        $start = clone $period->getStart();
        $start->add($dateInterval);

        return DateInterval::sum(
            $this->calculateWorkedPeriodWithNewWorkedPeriod($start , $period->getEnd()),
            $dateInterval
        );
    }

    /**
     * @param Harbinger\DateTime\WorkedPeriod $period
     * @return DateInterval
     * @throws \Harbinger\DateTime\RuntimeException If there's no worktime from next day
     **/
    private function calculateWorkedPeriodFromNextDay(Period $period)
    {
        $start = $this->goToNextDay($period->getStart());

        if ($period->getEnd() < $start) {
            throw new RuntimeException('Cannot calculate worked period');
        }

        return $this->calculateWorkedPeriodWithNewWorkedPeriod($start , $period->getEnd());
    }

    /**
     * @param \DateTimeInterface $start
     * @param \DateTimeInterface $end
     * @return DateInterval
     **/
    private function calculateWorkedPeriodWithNewWorkedPeriod(\DateTimeInterface $start , \DateTimeInterface $end)
    {
        try {
            $periodCollection = new Collection\Period();
            $periodCollection->add(new Period\DateTime($start , $end));

            return $this->calculateWorkedPeriod(new WorkedPeriod($periodCollection));
        } catch (RuntimeException $exception) {
            return new \DateInterval('PT0S');
        }
    }

    /**
     * Change a DateTimeInterface to next day
     * @param \DateTimeInterface $dateTime
     * @return \DateTimeInterface
     **/
    private function goToNextDay(\DateTimeInterface $dateTime)
    {
        $newDateTime = clone $dateTime;
        return $newDateTime->add(new \DateInterval('P1D'))->setTime(0 , 0);
    }
}
