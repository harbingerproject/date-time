<?php

namespace Harbinger\DateTime\Test;

use \Harbinger\DateTime\Calendar;
use \Harbinger\DateTime\Collection;
use \Harbinger\DateTime\Expedient;
use \Harbinger\DateTime\Period;
use \Harbinger\DateTime\WorkPeriod;
use \PHPUnit\Framework\TestCase;

class WorkPeriodTest extends TestCase
{

    /**
     * @var \Harbinger\DateTime\Period
     **/
    private $morning;

    /**
     * @var \Harbinger\DateTime\Period
     **/
    private $afternoon;

    /**
     * @var \Harbinger\Calendar
     **/
    private $brazilianCalendar;

    public function assertPreConditions()
    {
        $this->assertTrue(class_exists($class = WorkPeriod::class) , 'Class not found: '.$class);
    }

    public function setUp()
    {
        $this->morning = new Expedient(
            new Period\Decorator\Hour(
                new Period\DateTime(
                    \DateTime::createFromFormat('H:i:s' , '08:00:00'),
                    \DateTime::createFromFormat('H:i:s' , '12:00:00')
                )
            )
        );

        $this->morning->getCalendarDayCollection()->add(new Calendar\Day(Calendar\Day::MONDAY))
                                                  ->add(new Calendar\Day(Calendar\Day::TUESDAY))
                                                  ->add(new Calendar\Day(Calendar\Day::WEDNESDAY))
                                                  ->add(new Calendar\Day(Calendar\Day::THURSDAY))
                                                  ->add(new Calendar\Day(Calendar\Day::FRIDAY));

        $this->afternoon = new Expedient(
            new Period\Decorator\Hour(
                new Period\DateTime(
                    \DateTime::createFromFormat('H:i:s' , '13:30:00'),
                    \DateTime::createFromFormat('H:i:s' , '18:18:00')
                )
            )
        );

        $this->afternoon->getCalendarDayCollection()->add(new Calendar\Day(Calendar\Day::MONDAY))
                                                    ->add(new Calendar\Day(Calendar\Day::TUESDAY))
                                                    ->add(new Calendar\Day(Calendar\Day::WEDNESDAY))
                                                    ->add(new Calendar\Day(Calendar\Day::THURSDAY))
                                                    ->add(new Calendar\Day(Calendar\Day::FRIDAY));


        $this->internationalCalendar = new Calendar();

        $internationalHolidays = array();
        $internationalHolidays['new year\'s day'] = new Period\Decorator\Date\Month(
            new Period\Decorator\Date\Day(
                new Period\DateTime(
                    \DateTime::createFromFormat('m-d' , '01-01'),
                    \DateTime::createFromFormat('m-d' , '01-01')
                )
            )
        );

        $internationalHolidays['christmas holiday'] = new Period\Decorator\Date\Month(
            new Period\Decorator\Date\Day(
                new Period\DateTime(
                    \DateTime::createFromFormat('m-d' , '12-25'),
                    \DateTime::createFromFormat('m-d' , '12-25')
                )
            )
        );

        foreach ($internationalHolidays as $description => $holiday) {
            $this->internationalCalendar->getRecessCollection()->add(new Calendar\Recess($description , $holiday));
        }

        $this->brazilianCalendar = new Calendar();

        $brazilianHolidays = array();
        $brazilianHolidays['brazilian\'s independece day'] = new Period\Decorator\Date\Month(
            new Period\Decorator\Date\Day(
                new Period\DateTime(
                    \DateTime::createFromFormat('m-d' , '09-07'),
                    \DateTime::createFromFormat('m-d' , '09-07')
                )
            )
        );

        $brazilianHolidays['worke\'s day'] = new Period\Decorator\Date\Month(
            new Period\Decorator\Date\Day(
                new Period\DateTime(
                    \DateTime::createFromFormat('m-d' , '05-01'),
                    \DateTime::createFromFormat('m-d' , '05-01')
                )
            )
        );

        foreach ($brazilianHolidays as $description => $holiday) {
            $this->brazilianCalendar->getRecessCollection()->add(new Calendar\Recess($description , $holiday));
        }
    }

    public function testWorkPeriodMethodsWhatShouldExists()
    {
        $workPeriod = new WorkPeriod();

        $methods[] = 'setCalendarCollection';
        $methods[] = 'setExpedientCollection';
        $methods[] = 'getCalendarCollection';
        $methods[] = 'getExpedientCollection';
        $methods[] = 'isBetween';
        $methods[] = 'isExpedientDay';
        $methods[] = 'calculateWorkedPeriod';

        foreach ($methods as $method) {
            $this->assertTrue(method_exists($workPeriod , $method) , sprintf('Method %s not found' , $method));
        }
    }

    public function testWorkPeriodAddPeriodAndAddPeriodCollectionShouldWork()
    {
        $workPeriod = new WorkPeriod();

        $this->assertEquals(
            $expected = 0,
            $found = $workPeriod->getExpedientCollection()->count(),
            sprintf('Count should be %d, found %d' , $expected , $found)
        );

        $workPeriod->getExpedientCollection()->add($this->morning);
        $workPeriod->getExpedientCollection()->add($this->afternoon);

        $this->assertEquals(
            $expected = 2,
            $found = $workPeriod->getExpedientCollection()->count(),
            sprintf('Count should be %d, found %d' , $expected , $found)
        );

        $expedientCollection = new Collection\Expedient();

        $this->assertNotEquals($expedientCollection , $workPeriod->getExpedientCollection());
        $this->assertNotSame($expedientCollection , $workPeriod->getExpedientCollection());
        $this->assertEquals(
            $expected = 0,
            $found = $expedientCollection->count(),
            sprintf('Count should be %d, found %d' , $expected , $found)
        );

        $workPeriod->setExpedientCollection($expedientCollection);

        $this->assertEquals(
            $expected = 0,
            $found = $workPeriod->getExpedientCollection()->count(),
            sprintf('Count should be %d, found %d' , $expected , $found)
        );

        $expedientCollection->add($this->morning)
                            ->add($this->afternoon);


        $this->assertEquals(
            $expected = 2,
            $found = $expedientCollection->count(),
            sprintf('Count should be %d, found %d' , $expected , $found)
        );

        $this->assertEquals(
            $expected = 2,
            $found = $workPeriod->getExpedientCollection()->count(),
            sprintf('Count should be %d, found %d' , $expected , $found)
        );

        $this->assertSame($expedientCollection , $workPeriod->getExpedientCollection());

        return $workPeriod;
    }

    /**
     * @depends testWorkPeriodAddPeriodAndAddPeriodCollectionShouldWork
     **/
    public function testWorkPeriodAddCalendarAndAddCalendarCollectionShouldWork(WorkPeriod $workPeriod)
    {

        $this->assertEquals(
            $expected = 0,
            $found = $workPeriod->getCalendarCollection()->count(),
            sprintf('Count should be %d, found %d' , $expected , $found)
        );

        $workPeriod->getCalendarCollection()->add($this->internationalCalendar);
        $workPeriod->getCalendarCollection()->add($this->brazilianCalendar);

        $this->assertEquals(
            $expected = 2,
            $found = $workPeriod->getCalendarCollection()->count(),
            sprintf('Count should be %d, found %d' , $expected , $found)
        );

        $calendarCollection = new Collection\Calendar();

        $this->assertNotEquals($calendarCollection , $workPeriod->getCalendarCollection());
        $this->assertNotSame($calendarCollection , $workPeriod->getCalendarCollection());
        $this->assertEquals(
            $expected = 0,
            $found = $calendarCollection->count(),
            sprintf('Count should be %d, found %d' , $expected , $found)
        );

        $workPeriod->setCalendarCollection($calendarCollection);

        $this->assertEquals(
            $expected = 0,
            $found = $workPeriod->getCalendarCollection()->count(),
            sprintf('Count should be %d, found %d' , $expected , $found)
        );

        $calendarCollection->add($this->internationalCalendar);
        $calendarCollection->add($this->brazilianCalendar);

        $this->assertEquals(
            $expected = 2,
            $found = $calendarCollection->count(),
            sprintf('Count should be %d, found %d' , $expected , $found)
        );

        $this->assertEquals(
            $expected = 2,
            $found = $workPeriod->getCalendarCollection()->count(),
            sprintf('Count should be %d, found %d' , $expected , $found)
        );

        $this->assertSame($calendarCollection , $workPeriod->getCalendarCollection());

        return $workPeriod;
    }

    /**
     * @depends testWorkPeriodAddCalendarAndAddCalendarCollectionShouldWork
     **/
    public function testDateTimeIsAWorkDayShouldWork(WorkPeriod $workPeriod)
    {
        $this->assertTrue($workPeriod->isExpedientDay(new \DateTime('2014-12-16')));
        $this->assertFalse($workPeriod->isExpedientDay(new \DateTime('2014-12-14')));
        $this->assertFalse($workPeriod->isExpedientDay(new \DateTime('2015-05-01')));
        $this->assertFalse($workPeriod->isExpedientDay(new \DateTime('2015-01-01')));
        $this->assertFalse($workPeriod->isExpedientDay(new \DateTime('2015-12-25')));
        $this->assertFalse($workPeriod->isExpedientDay(new \DateTime('2015-09-07')));
    }

    /**
     * @depends testWorkPeriodAddCalendarAndAddCalendarCollectionShouldWork
     **/
    public function testDateTimeIsABetweenShouldWork(WorkPeriod $workPeriod)
    {

        $shouldBe = [
            new \DateTime('2014-12-16 08:00:00'),
            new \DateTime('2014-12-16 09:00:00'),
            new \DateTime('2014-12-16 11:59:59'),
            new \DateTime('2014-12-16 13:30:00'),
            new \DateTime('2014-12-16 17:00:00'),
            new \DateTime('2014-12-16 18:17:59')
        ];

        $shouldntBe = [
            new \DateTime('2014-12-16 07:59:59'),
            new \DateTime('2014-12-16 12:00:00'),
            new \DateTime('2014-12-16 12:00:01'),
            new \DateTime('2014-12-16 00:00:00'),
            new \DateTime('2014-12-16 13:29:59'),
            new \DateTime('2014-12-16 18:18:00'),
            new \DateTime('2014-12-16 18:18:01'),
            new \DateTime('2014-12-16 00:00:00'),
            new \DateTime('2015-05-01 17:00:00'),
            new \DateTime('2010-05-01 17:00:00')
        ];

        foreach ($shouldBe as $date) {
            $this->assertTrue($workPeriod->isBetween($date) , sprintf('%s should be between' , print_r($date , true)));
        };

        foreach ($shouldntBe as $date) {
            $this->assertFalse(
                $workPeriod->isBetween($date),
                sprintf("%s shouldn't be between" , print_r($date , true))
            );
        }
    }

    /**
     * @depends testWorkPeriodAddCalendarAndAddCalendarCollectionShouldWork
     */
    public function testWorkPeriodWorkedPeriodFullTime(\Harbinger\DateTime\WorkPeriod $workPeriod)
    {
        $periodCollection = new Collection\Period();
        $periodCollection->add(
            new Period\DateTime(
                new \DateTime('2014-08-18 08:00:00'),
                new \DateTime('2014-08-18 18:18:00')
            )
        );

        $workedDateInterval = $workPeriod->calculateWorkedPeriod(
            new \Harbinger\DateTime\WorkedPeriod($periodCollection)
        );

        $this->assertEquals(new \DateInterval('PT8H48M') , $workedDateInterval);
        $this->assertSame('08:48:00' , $workedDateInterval->format('%H:%I:%S'));
    }


    /**
     * @depends testWorkPeriodAddCalendarAndAddCalendarCollectionShouldWork
     **/
    public function testWorkPeriodWorkedPeriodHalfTimeFullMorning(\Harbinger\DateTime\WorkPeriod $workPeriod)
    {
        $periodCollection = new Collection\Period();
        $periodCollection->add(
            new Period\DateTime(
                new \DateTime('2014-08-18 08:00:00'),
                new \DateTime('2014-08-18 12:00:00')
            )
        );

        $workedDateInterval = $workPeriod->calculateWorkedPeriod(
            new \Harbinger\DateTime\WorkedPeriod($periodCollection)
        );

        $this->assertEquals(new \DateInterval('PT4H') , $workedDateInterval);
        $this->assertSame('04:00:00' , $workedDateInterval->format('%H:%I:%S'));
    }

    /**
     * @depends testWorkPeriodAddCalendarAndAddCalendarCollectionShouldWork
     **/
    public function testWorkPeriodWorkedPeriodHalfTimeFullAfternoon(\Harbinger\DateTime\WorkPeriod $workPeriod)
    {
        $periodCollection = new Collection\Period();
        $periodCollection->add(
            new Period\DateTime(
                new \DateTime('2014-08-18 13:30:00'),
                new \DateTime('2014-08-18 18:18:00')
            )
        );

        $workedDateInterval = $workPeriod->calculateWorkedPeriod(
            new \Harbinger\DateTime\WorkedPeriod($periodCollection)
        );

        $this->assertEquals(new \DateInterval('PT4H48M') , $workedDateInterval);
        $this->assertSame('04:48:00' , $workedDateInterval->format('%H:%I:%S'));
    }

    /**
     * @depends testWorkPeriodAddCalendarAndAddCalendarCollectionShouldWork
     **/
    public function testWorkPeriodWorkedPeriodHalfTime(\Harbinger\DateTime\WorkPeriod $workPeriod)
    {
        $periodCollection = new Collection\Period();
        $periodCollection->add(
            new Period\DateTime(
                new \DateTime('2014-08-18 10:00:00'),
                new \DateTime('2014-08-18 15:15:00')
            )
        );

        $workedDateInterval = $workPeriod->calculateWorkedPeriod(
            new \Harbinger\DateTime\WorkedPeriod($periodCollection)
        );

        $this->assertEquals(new \DateInterval('PT3H45M') , $workedDateInterval);
        $this->assertSame('03:45:00' , $workedDateInterval->format('%H:%I:%S'));
    }

    /**
     * @depends testWorkPeriodAddCalendarAndAddCalendarCollectionShouldWork
     **/
    public function testWorkPeriodWorkedPeriodHalfTimeHalfMorning(\Harbinger\DateTime\WorkPeriod $workPeriod)
    {
        $periodCollection = new Collection\Period();
        $periodCollection->add(
            new Period\DateTime(
                new \DateTime('2014-08-18 09:00:00'),
                new \DateTime('2014-08-18 11:15:00')
            )
        );

        $workedDateInterval = $workPeriod->calculateWorkedPeriod(
            new \Harbinger\DateTime\WorkedPeriod($periodCollection)
        );

        $this->assertEquals(new \DateInterval('PT2H15M') , $workedDateInterval);
        $this->assertSame('02:15:00' , $workedDateInterval->format('%H:%I:%S'));
    }

    /**
     * @depends testWorkPeriodAddCalendarAndAddCalendarCollectionShouldWork
     **/
    public function testWorkPeriodWorkedPeriodHalTimeHalfAfternoon(\Harbinger\DateTime\WorkPeriod $workPeriod)
    {
        $periodCollection = new Collection\Period();
        $periodCollection->add(
            new Period\DateTime(
                new \DateTime('2014-08-18 14:30:00'),
                new \DateTime('2014-08-18 15:30:00')
            )
        );

        $workedDateInterval = $workPeriod->calculateWorkedPeriod(
            new \Harbinger\DateTime\WorkedPeriod($periodCollection)
        );

        $this->assertEquals(new \DateInterval('PT1H') , $workedDateInterval);
        $this->assertSame('01:00:00' , $workedDateInterval->format('%H:%I:%S'));
    }

    /**
     * @depends testWorkPeriodAddCalendarAndAddCalendarCollectionShouldWork
     * @expectedException \RuntimeException
     **/
    public function testWorkPeriodWorkedPeriodException(\Harbinger\DateTime\WorkPeriod $workPeriod)
    {
        $periodCollection = new Collection\Period();
        $periodCollection->add(
            new Period\DateTime(
                new \DateTime('2014-08-19 09:31:00'),
                new \DateTime('2014-08-19 09:30:00')
            )
        );

        $workedDateInterval = $workPeriod->calculateWorkedPeriod(
            new \Harbinger\DateTime\WorkedPeriod($periodCollection)
        );
    }

    /**
     * @depends testWorkPeriodAddCalendarAndAddCalendarCollectionShouldWork
     */
    public function testWorkPeriodWorkedPeriodZero(\Harbinger\DateTime\WorkPeriod $workPeriod)
    {
        $periodCollection = new Collection\Period();
        $periodCollection->add(
            new Period\DateTime(
                new \DateTime('2014-08-18 08:00:00'),
                new \DateTime('2014-08-18 08:00:00')
            )
        );

        $workedDateInterval = $workPeriod->calculateWorkedPeriod(
            new \Harbinger\DateTime\WorkedPeriod($periodCollection)
        );

        $this->assertEquals(new \DateInterval('PT0S') , $workedDateInterval);
        $this->assertSame('00:00:00' , $workedDateInterval->format('%H:%I:%S'));
    }

    /**
     * @depends testWorkPeriodAddCalendarAndAddCalendarCollectionShouldWork
     */
    public function testWorkPeriodWorkedPeriodOneSecond(\Harbinger\DateTime\WorkPeriod $workPeriod)
    {
        $periodCollection = new Collection\Period();
        $periodCollection->add(
            new Period\DateTime(
                new \DateTime('2014-08-18 08:00:00'),
                new \DateTime('2014-08-18 08:00:01')
            )
        );

        $workedDateInterval = $workPeriod->calculateWorkedPeriod(
            new \Harbinger\DateTime\WorkedPeriod($periodCollection)
        );

        $this->assertEquals(new \DateInterval('PT1S') , $workedDateInterval);
        $this->assertSame('00:00:01' , $workedDateInterval->format('%H:%I:%S'));
    }

    /**
     * @depends testWorkPeriodAddCalendarAndAddCalendarCollectionShouldWork
     */
    public function testWorkPeriodWorkedPeriodOneWeek(\Harbinger\DateTime\WorkPeriod $workPeriod)
    {
        $periodCollection = new Collection\Period();
        $periodCollection->add(
            new Period\DateTime(
                new \DateTime('2015-06-22 08:00:00'),
                new \DateTime('2015-06-26 18:18:00')
            )
        );

        $workedDateInterval = $workPeriod->calculateWorkedPeriod(
            new \Harbinger\DateTime\WorkedPeriod($periodCollection)
        );

        $this->assertSame('1 20:00:00' , $workedDateInterval->format('%a %H:%I:%S'));
    }

    /**
     * @depends testWorkPeriodAddCalendarAndAddCalendarCollectionShouldWork
     */
    public function testWorkPeriodWorkedPeriodTwoWeek(\Harbinger\DateTime\WorkPeriod $workPeriod)
    {
        $periodCollection = new Collection\Period();
        $periodCollection->add(
            new Period\DateTime(
                new \DateTime('2015-06-15 08:00:00'),
                new \DateTime('2015-06-26 18:18:00')
            )
        );

        $workedDateInterval = $workPeriod->calculateWorkedPeriod(
            new \Harbinger\DateTime\WorkedPeriod($periodCollection)
        );

        $this->assertSame('3 16:00:00' , $workedDateInterval->format('%a %H:%I:%S'));
    }

    /**
     * @depends testWorkPeriodAddCalendarAndAddCalendarCollectionShouldWork
     */
    public function testWorkPeriodWorkedPeriodAMonth(\Harbinger\DateTime\WorkPeriod $workPeriod)
    {
        $periodCollection = new Collection\Period();
        $periodCollection->add(
            new Period\DateTime(
                new \DateTime('2015-06-01 08:00:00'),
                new \DateTime('2015-06-30 18:18:00')
            )
        );

        $workedDateInterval = $workPeriod->calculateWorkedPeriod(
            new \Harbinger\DateTime\WorkedPeriod($periodCollection)
        );

        $this->assertSame('8 01:36:00' , $workedDateInterval->format('%a %H:%I:%S'));
    }

    /**
     * @depends testWorkPeriodAddCalendarAndAddCalendarCollectionShouldWork
     */
    public function testWorkPeriodWorkedPeriodFridayToMonday(\Harbinger\DateTime\WorkPeriod $workPeriod)
    {
        $periodCollection = new Collection\Period();
        $periodCollection->add(
            new Period\DateTime(
                new \DateTime('2014-12-19 08:00:00'),
                new \DateTime('2014-12-22 18:18:00')
            )
        );

        $workedDateInterval = $workPeriod->calculateWorkedPeriod(
            new \Harbinger\DateTime\WorkedPeriod($periodCollection)
        );

        $this->assertEquals(new \DateInterval('PT17H36M') , $workedDateInterval);
        $this->assertSame('17:36:00' , $workedDateInterval->format('%H:%I:%S'));
    }

    /**
     * @depends testWorkPeriodAddCalendarAndAddCalendarCollectionShouldWork
     */
    public function testWorkPeriodWorkedPeriodSaturdayToSunday(\Harbinger\DateTime\WorkPeriod $workPeriod)
    {
        $periodCollection = new Collection\Period();
        $periodCollection->add(
            new Period\DateTime(
                new \DateTime('2014-12-20 08:00:00'),
                new \DateTime('2014-12-21 18:18:00')
            )
        );

        $workedDateInterval = $workPeriod->calculateWorkedPeriod(
            new \Harbinger\DateTime\WorkedPeriod($periodCollection)
        );

        $this->assertEquals(new \DateInterval('PT0S') , $workedDateInterval);
        $this->assertSame('00:00:00' , $workedDateInterval->format('%H:%I:%S'));
    }

    /**
     * @depends testWorkPeriodAddCalendarAndAddCalendarCollectionShouldWork
     */
    public function testWorkPeriodWorkedPeriodMondayToFridayOnlyMorning(\Harbinger\DateTime\WorkPeriod $workPeriod)
    {
        $periodCollection = new Collection\Period();
        $periodCollection->add(
            new Period\DateTime(
                new \DateTime('2015-12-14 08:00:00'),
                new \DateTime('2015-12-14 12:00:00')
            )
        );
        $periodCollection->add(
            new Period\DateTime(
                new \DateTime('2015-12-15 08:00:00'),
                new \DateTime('2015-12-15 12:00:00')
            )
        );
        $periodCollection->add(
            new Period\DateTime(
                new \DateTime('2015-12-16 08:00:00'),
                new \DateTime('2015-12-16 12:00:00')
            )
        );
        $periodCollection->add(
            new Period\DateTime(
                new \DateTime('2015-12-17 08:00:00'),
                new \DateTime('2015-12-17 12:00:00')
            )
        );
        $periodCollection->add(
            new Period\DateTime(
                new \DateTime('2015-12-18 08:00:00'),
                new \DateTime('2015-12-18 12:00:00')
            )
        );

        $workedDateInterval = $workPeriod->calculateWorkedPeriod(
            new \Harbinger\DateTime\WorkedPeriod($periodCollection)
        );

        $this->assertEquals(new \DateInterval('PT20H') , $workedDateInterval);
        $this->assertSame('20:00:00' , $workedDateInterval->format('%H:%I:%S'));
    }
}
