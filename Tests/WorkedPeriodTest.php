<?php

namespace Harbinger\DateTime\Test;

use \Harbinger\DateTime\CalendarDay;
use \Harbinger\DateTime\Collection;
use \Harbinger\DateTime\Period;
use \Harbinger\DateTime\WorkedPeriod;
use \PHPUnit\Framework\TestCase;

class WorkedPeriodTest extends TestCase
{

    /**
     * @var \Harbinger\DateTime\Period
     **/
    private $morning;

    /**
     * @var \Harbinger\DateTime\Period
     **/
    private $afternoon;

    public function assertPreConditions()
    {
        $this->assertTrue(class_exists($class = WorkedPeriod::class) , 'Class not found: '.$class);
    }

    public function testConstructWorkedPeriodWithValidArgumentsShouldWork()
    {
        $start = \DateTime::createFromFormat('H:i:s' , '08:00:00');
        $end = \DateTime::createFromFormat('H:i:s' , '12:00:00');

        $period = new Period\DateTime($start , $end);

        $periodCollection = new Collection\Period();
        $periodCollection->add($period);

        $this->assertInstanceOf(
            $instance = WorkedPeriod::class,
            $class = new WorkedPeriod($periodCollection),
            sprintf('%s should be instance of %s' , get_class($class) , $instance)
        );
    }

    /**
     * @depends testConstructWorkedPeriodWithValidArgumentsShouldWork
     **/
    public function testGetPeriodShouldWork()
    {
        $start = \DateTime::createFromFormat('H:i:s' , '08:00:00');
        $end = \DateTime::createFromFormat('H:i:s' , '12:00:00');

        $periodCollection = new Collection\Period();
        $periodCollection->add(new Period\DateTime($start , $end));
        $workedPeriod = new WorkedPeriod($periodCollection);

        $this->assertSame($periodCollection , $workedPeriod->getPeriodCollection());

        $workedPeriod = clone $workedPeriod;
        $this->assertEquals($periodCollection , $workedPeriod->getPeriodCollection());
        $this->assertNotSame($periodCollection , $workedPeriod->getPeriodCollection());

    }
}
