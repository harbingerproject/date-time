<?php

namespace Harbinger\DateTime\Test;

use \Harbinger\DateTime\Calendar;
use \Harbinger\DateTime\Collection;
use \Harbinger\DateTime\Period;
use \PHPUnit\Framework\TestCase;

class CalendarTest extends TestCase
{

    public function assertPreConditions()
    {
        $this->assertTrue(class_exists($class = Calendar::class) , 'Class not found: '.$class);
    }

    public function setUp()
    {

    }

    public function testCalendarMethodsWhatShouldExists()
    {
        $calendar = new Calendar();

        $methods[] = 'getRecessCollection';
        $methods[] = 'setRecessCollection';

        foreach ($methods as $method) {
            $this->assertTrue(method_exists($calendar , $method) , sprintf('Method %s not found' , $method));
        }
    }

    /**
     * @depends testCalendarMethodsWhatShouldExists
     **/
    public function testMethodSetReccessCollectionShouldWork()
    {
        $calendar = new Calendar(new Collection\Calendar\Recess());

        $calendar->setRecessCollection(new Collection\Calendar\Recess());
    }

    /**
     * @depends testCalendarMethodsWhatShouldExists
     * @depends testMethodSetReccessCollectionShouldWork
     **/
    public function testMethodGetWorkPeriodShouldWork()
    {
        $calendar = new Calendar($recessCollection = new Collection\Calendar\Recess());

        $instances[] = \Iterator::class;
        $instances[] = \Harbinger\Iterator\Collection::class;
        $instances[] = \Harbinger\DateTime\Collection\Calendar\Recess::class;

        foreach ($instances as $instance) {
            $this->assertInstanceOf(
                $instance,
                $class = $calendar->getRecessCollection(),
                sprintf('%s should be an instance of %s' , print_r($class , true) , $instance)
            );
        }

        $this->assertSame(
            $calendar->getRecessCollection(),
            $recessCollection,
            sprintf(
                '%s should be same as %s',
                print_r($calendar->getRecessCollection() , true),
                print_r($recessCollection , true)
            )
        );
    }

    /**
     * @depends testMethodGetWorkPeriodShouldWork
     **/
    public function testMethodIsRecessShouldWork()
    {
        $calendar = new Calendar();

        $holidays = array();
        $holidays['new year\'s day'] = new Period\Decorator\Date\Month(
            new Period\Decorator\Date\Day(
                new Period\DateTime(
                    \DateTime::createFromFormat('m-d' , '01-01'),
                    \DateTime::createFromFormat('m-d' , '01-01')
                )
            )
        );

        $holidays['brazilian\'s independece day'] = new Period\Decorator\Date\Month(
            new Period\Decorator\Date\Day(
                new Period\DateTime(
                    \DateTime::createFromFormat('m-d' , '09-07'),
                    \DateTime::createFromFormat('m-d' , '09-07')
                )
            )
        );

        $holidays['christmas holiday'] = new Period\Decorator\Date\Month(
            new Period\Decorator\Date\Day(
                new Period\DateTime(
                    \DateTime::createFromFormat('m-d' , '12-25'),
                    \DateTime::createFromFormat('m-d' , '12-25')
                )
            )
        );

        $holidays['worke\'s day'] = new Period\Decorator\Date\Month(
            new Period\Decorator\Date\Day(
                new Period\DateTime(
                    \DateTime::createFromFormat('m-d' , '05-01'),
                    \DateTime::createFromFormat('m-d' , '05-01')
                )
            )
        );

        foreach ($holidays as $description => $holiday) {
            $calendar->getRecessCollection()->add(new Calendar\Recess($description , $holiday));
        }

        $this->assertTrue(
            $calendar->isRecess($date = new \DateTime('2015-01-01')),
            sprintf('Date %s should be a recess' , $date->format('Y-m-d'))
        );

        $this->assertFalse(
            $calendar->isRecess($date = new \DateTime('2015-01-02')),
            sprintf('Date %s should not be a recess' , $date->format('Y-m-d'))
        );

        $this->assertTrue(
            $calendar->isRecess($date = new \DateTime('2000-01-01')),
            sprintf('Date %s should be a recess' , $date->format('Y-m-d'))
        );

        $this->assertTrue(
            $calendar->isRecess($date = new \DateTime('1900-01-01')),
            sprintf('Date %s should be a recess' , $date->format('Y-m-d'))
        );

        $this->assertFalse(
            $calendar->isRecess($date = new \DateTime('1900-01-02')),
            sprintf('Date %s should not be a recess' , $date->format('Y-m-d'))
        );

        $this->assertFalse(
            $calendar->isRecess($date = new \DateTime('1900-02-01')),
            sprintf('Date %s should not be a recess' , $date->format('Y-m-d'))
        );

        $this->assertTrue(
            $calendar->isRecess($date = new \DateTime('1900-09-07')),
            sprintf('Date %s should be a recess' , $date->format('Y-m-d'))
        );

        $this->assertTrue(
            $calendar->isRecess($date = new \DateTime('2050-09-07')),
            sprintf('Date %s should be a recess' , $date->format('Y-m-d'))
        );

        $this->assertFalse(
            $calendar->isRecess($date = new \DateTime('20015-07-07')),
            sprintf('Date %s should not be a recess' , $date->format('Y-m-d'))
        );

        $this->assertTrue(
            $calendar->isRecess($date = new \DateTime('1900-12-25')),
            sprintf('Date %s should be a recess' , $date->format('Y-m-d'))
        );

        $this->assertTrue(
            $calendar->isRecess($date = new \DateTime('2050-12-25')),
            sprintf('Date %s should be a recess' , $date->format('Y-m-d'))
        );

        $this->assertFalse(
            $calendar->isRecess($date = new \DateTime('20015-11-25')),
            sprintf('Date %s should not be a recess' , $date->format('Y-m-d'))
        );

        $this->assertTrue(
            $calendar->isRecess($date = new \DateTime('2050-05-01')),
            sprintf('Date %s should be a recess' , $date->format('Y-m-d'))
        );

        $this->assertTrue(
            $calendar->isRecess($date = new \DateTime('2015-05-01')),
            sprintf('Date %s should be a recess' , $date->format('Y-m-d'))
        );
    }
}
