<?php

namespace Harbinger\DateTime\Test\Calendar;

use \Harbinger\DateTime\Calendar\Recess;
use \PHPUnit\Framework\TestCase;

class RecessTest extends TestCase
{

    public function assertPreConditions()
    {
        $this->assertTrue(class_exists($class = Recess::class) , 'Class not found: '.$class);
    }

    public function setUp()
    {
    }

    public function testConstantShouldExists()
    {
    }

    /**
     * @depends testConstantShouldExists
     **/
    public function testConstantValuesShouldMatchWithDateTimeWeekDaysValues()
    {
    }
}
