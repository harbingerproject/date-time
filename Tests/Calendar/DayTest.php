<?php

namespace Harbinger\DateTime\Test\Calendar;

use \Harbinger\DateTime\Calendar\Day;
use \PHPUnit\Framework\TestCase;

class DayTest extends TestCase
{

    public function assertPreConditions()
    {
        $this->assertTrue(class_exists($class = Day::class) , 'Class not found: '.$class);
    }

    public function setUp()
    {

    }

    public function testConstantShouldExists()
    {
        $this->assertTrue(defined(Day::class.'::SUNDAY')  , 'There\'s no SUNDAY constant');
        $this->assertTrue(defined(Day::class.'::MONDAY')  , 'There\'s no MONDAY constant');
        $this->assertTrue(defined(Day::class.'::TUESDAY')  , 'There\'s no TUESDAY constant');
        $this->assertTrue(defined(Day::class.'::WEDNESDAY')  , 'There\'s no WEDNESDAY constant');
        $this->assertTrue(defined(Day::class.'::THURSDAY')  , 'There\'s no THURSDAY constant');
        $this->assertTrue(defined(Day::class.'::FRIDAY')  , 'There\'s no FRIDAY constant');
        $this->assertTrue(defined(Day::class.'::SATURDAY')  , 'There\'s no SATURDAY constant');
    }

    /**
     * @depends testConstantShouldExists
     **/
    public function testConstantValuesShouldMatchWithDateTimeWeekDaysValues()
    {
        $sunday = new \DateTime('next sunday');
        $this->assertTrue(Day::SUNDAY == $sunday->format('w') , 'Value of sunday weekday not match');

        $sunday = new \DateTime('next monday');
        $this->assertTrue(Day::MONDAY == $sunday->format('w') , 'Value of monday weekday not match');

        $sunday = new \DateTime('next tuesday');
        $this->assertTrue(Day::TUESDAY == $sunday->format('w') , 'Value of tuesday weekday not match');

        $sunday = new \DateTime('next wednesday');
        $this->assertTrue(Day::WEDNESDAY == $sunday->format('w') , 'Value of wednesday weekday not match');

        $sunday = new \DateTime('next thursday');
        $this->assertTrue(Day::THURSDAY == $sunday->format('w') , 'Value of thursday weekday not match');

        $sunday = new \DateTime('next friday');
        $this->assertTrue(Day::FRIDAY == $sunday->format('w') , 'Value of friday weekday not match');

        $sunday = new \DateTime('next saturday');
        $this->assertTrue(Day::SATURDAY == $sunday->format('w') , 'Value of saturday weekday not match');
    }
}
