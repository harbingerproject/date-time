<?php

namespace Harbinger\DateTime\Test\Period\Decorator;

use \Harbinger\DateTime\Period;
use \PHPUnit\Framework\TestCase;

class HourTest extends TestCase
{

    public function assertPreConditions()
    {
        $this->assertTrue(class_exists($class = Period\Decorator\Date::class) , 'Class not found: '.$class);
    }

    public function testCreateAPeriodWithValidArgumentsShouldWork()
    {
        $start = new \DateTime('10:00:00');
        $end = new \DateTime('20:00:00');

        $period = new Period\DateTime($start , $end);
        $hour = new Period\Decorator\Hour($period);

        $this->assertInstanceOf(
            $instance = Period::class,
            $hour,
            sprintf("Should be instance of %s" , $instance)
        );

        $this->assertInstanceOf(
            $instance = Period\Decorator::class,
            $hour ,
            sprintf("Should be instance of %s" , $instance)
        );

        $this->assertInstanceOf(
            $instance = Period\AbstractDecorator::class,
            $hour,
            sprintf("Should be instance of %s" , $instance)
        );

        $this->assertTrue($hour->isBetween(new \DateTime('10:00:00')));
        $this->assertTrue($hour->isBetween(new \DateTime('19:59:59')));
        $this->assertFalse($hour->isBetween(new \DateTime('09:59:59')));
        $this->assertFalse($hour->isBetween(new \DateTime('20:00:00')));
    }
}
