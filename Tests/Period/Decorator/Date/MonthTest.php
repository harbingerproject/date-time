<?php

namespace Harbinger\DateTime\Test\Period\Decorator\Date;

use \Harbinger\DateTime\Period;
use \Harbinger\DateTime\Period\Decorator;
use \PHPUnit\Framework\TestCase;

class MonthTest extends TestCase
{

    public function assertPreConditions()
    {
        $this->assertTrue(class_exists($class = Decorator\Date\Month::class) , 'Class not found: '.$class);
    }

    public function setUp()
    {
        date_default_timezone_set('America/Sao_Paulo');
    }

    public function testCreateAPeriodWithValidArgumentsShouldWork()
    {
        $start = new \DateTime('now');
        $end = new \DateTime('now');

        $period = new Period\DateTime($start , $end);
        $month = new Decorator\Date\Month($period);

        $this->assertInstanceOf(
            $instance = Period::class,
            $month,
            sprintf("Should be instance of %s" , $instance)
        );

        $this->assertInstanceOf(
            $instance = Period\Decorator::class,
            $month ,
            sprintf("Should be instance of %s" , $instance)
        );

        $this->assertInstanceOf(
            $instance = Period\AbstractDecorator::class,
            $month,
            sprintf("Should be instance of %s" , $instance)
        );

        $this->assertTrue(
            $month->isBetween($data = new \DateTime()),
            sprintf('%s should be between' , print_r($data , true))
        );
        $this->assertTrue(
            $month->isBetween($data = new \DateTime('last day of this month')),
            sprintf('%s should be between' , print_r($data , true))
        );
        $this->assertTrue(
            $month->isBetween($data = new \DateTime('+1 year')),
            sprintf('%s should be between' , print_r($data , true))
        );
        $this->assertTrue(
            $month->isBetween($data = new \DateTime('-100 year')),
            sprintf('%s should be between' , print_r($data , true))
        );
        $this->assertFalse(
            $month->isBetween($data = new \DateTime('+1 month')),
            sprintf('%s shouldn\'t be between' , print_r($data , true))
        );
        $this->assertFalse(
            $month->isBetween($data = new \DateTime('+1 year +1 month')),
            sprintf('%s shouldn\'t be between' , print_r($data , true))
        );
        $this->assertFalse(
            $month->isBetween($data = new \DateTime('-100 year +1 month +1 day')),
            sprintf('%s shouldn\'t be between' , print_r($data , true))
        );
        $this->assertFalse(
            $month->isBetween($data = new \DateTime('-100 year -1 month -1 day')),
            sprintf('%s shouldn\'t be between' , print_r($data , true))
        );
    }

    public function testCreateAPeriodWithMethodCreateFromFormatFromDateTimeClassValidArgumentsShouldWork()
    {
        $start = \DateTime::createFromFormat('m' , '01');
        $end = \DateTime::createFromFormat('m-d' , '02-28');

        $period = new Period\DateTime($start , $end);
        $month = new Decorator\Date\Month($period);

        $this->assertTrue(
            $month->isBetween($data = new \DateTime('2014-01-01')),
            sprintf('%s should be between' , print_r($data , true))
        );
        $this->assertTrue(
            $month->isBetween($data = new \DateTime('2015-01-30')),
            sprintf('%s should be between' , print_r($data , true))
        );
        $this->assertTrue(
            $month->isBetween($data = new \DateTime('2015-02-28')),
            sprintf('%s should be between' , print_r($data , true))
        );
        $this->assertTrue(
            $month->isBetween($data = new \DateTime('1900-01-01')),
            sprintf('%s should be between' , print_r($data , true))
        );
        $this->assertFalse(
            $month->isBetween($data = new \DateTime('2015-03-01')),
            sprintf('%s shouldn\'t be between' , print_r($data , true))
        );
        $this->assertFalse(
            $month->isBetween(new \DateTime('1900-12-31')),
            sprintf('%s shouldn\'t be between' , print_r($data , true))
        );
    }
}
