<?php

namespace Harbinger\DateTime\Test\Period\Decorator\Date;

use \Harbinger\DateTime\Period;
use \Harbinger\DateTime\Period\Decorator;
use \PHPUnit\Framework\TestCase;

class YearTest extends TestCase
{

    public function assertPreConditions()
    {
        $this->assertTrue(class_exists($class = Decorator\Date\Year::class) , 'Class not found: '.$class);
    }

    public function testCreateAPeriodWithValidArgumentsShouldWork()
    {
        $start = new \DateTime('now');
        $end = new \DateTime('now');

        $period = new Period\DateTime($start , $end);
        $day = new Decorator\Date\Year($period);

        $this->assertInstanceOf(
            $instance = Period::class,
            $day,
            sprintf("Should be instance of %s" , $instance)
        );

        $this->assertInstanceOf(
            $instance = Period\Decorator::class,
            $day ,
            sprintf("Should be instance of %s" , $instance)
        );

        $this->assertInstanceOf(
            $instance = Period\AbstractDecorator::class,
            $day,
            sprintf("Should be instance of %s" , $instance)
        );

        $this->assertTrue($day->isBetween(new \DateTime()));
        $this->assertTrue($day->isBetween(new \DateTime('+1 day')));
        $this->assertFalse($day->isBetween(new \DateTime('+1 year')));
        $this->assertFalse($day->isBetween(new \DateTime('-100 year')));
        $this->assertFalse($day->isBetween(new \DateTime('-100 year -1 day')));
    }

    public function testCreateAPeriodWithMethodCreateFromFormatFromDateTimeClassValidArgumentsShouldWork()
    {
        $start = \DateTime::createFromFormat('Y' , '1950');
        $end = \DateTime::createFromFormat('Y' , '2000');

        $period = new Period\DateTime($start , $end);
        $day = new Decorator\Date\Year($period);

        $this->assertFalse($day->isBetween(new \DateTime('2014-01-01')));
        $this->assertFalse($day->isBetween(new \DateTime('2015-01-30')));
        $this->assertFalse($day->isBetween(new \DateTime('2015-02-28')));
        $this->assertFalse($day->isBetween(new \DateTime('1900-01-01')));
        $this->assertTrue($day->isBetween(new \DateTime('1950-01-01')));
        $this->assertTrue($day->isBetween(new \DateTime('1950-12-31')));
        $this->assertFalse($day->isBetween(new \DateTime('2015-03-01')));
        $this->assertFalse($day->isBetween(new \DateTime('2015-12-10')));
        $this->assertTrue($day->isBetween(new \DateTime('2000-12-31')));
        $this->assertFalse($day->isBetween(new \DateTime('2001-01-01')));
        $this->assertFalse($day->isBetween(new \DateTime('1900-12-31')));
    }
}
