<?php

namespace Harbinger\DateTime\Test\Period\Decorator\Date;

use \Harbinger\DateTime\Period;
use \Harbinger\DateTime\Period\Decorator;
use \PHPUnit\Framework\TestCase;

class DayTest extends TestCase
{

    public function assertPreConditions()
    {
        $this->assertTrue(class_exists($class = Decorator\Date\Day::class) , 'Class not found: '.$class);
    }

    public function testCreateAPeriodWithValidArgumentsShouldWork()
    {
        $start = new \DateTime('now');
        $end = new \DateTime('now');

        $period = new Period\DateTime($start , $end);
        $day = new Decorator\Date\Day($period);

        $this->assertInstanceOf(
            $instance = Period::class,
            $day,
            sprintf("Should be instance of %s" , $instance)
        );

        $this->assertInstanceOf(
            $instance = Period\Decorator::class,
            $day ,
            sprintf("Should be instance of %s" , $instance)
        );

        $this->assertInstanceOf(
            $instance = Period\AbstractDecorator::class,
            $day,
            sprintf("Should be instance of %s" , $instance)
        );

        $this->assertTrue($day->isBetween(new \DateTime()));
        $this->assertFalse($day->isBetween(new \DateTime('+1 day')));
        $this->assertTrue($day->isBetween(new \DateTime('+1 year')));
        $this->assertTrue($day->isBetween(new \DateTime('-100 year')));
        $this->assertFalse($day->isBetween(new \DateTime('-100 year -1 day')));
    }

    public function testCreateAPeriodWithMethodCreateFromFormatFromDateTimeClassValidArgumentsShouldWork()
    {
        $start = \DateTime::createFromFormat('d' , '01');
        $end = \DateTime::createFromFormat('d' , '10');

        $period = new Period\DateTime($start , $end);
        $day = new Decorator\Date\Day($period);

        $this->assertTrue($day->isBetween(new \DateTime('2014-01-01')));
        $this->assertFalse($day->isBetween(new \DateTime('2015-01-30')));
        $this->assertFalse($day->isBetween(new \DateTime('2015-02-28')));
        $this->assertTrue($day->isBetween(new \DateTime('1900-01-01')));
        $this->assertTrue($day->isBetween(new \DateTime('2015-03-01')));
        $this->assertTrue($day->isBetween(new \DateTime('2015-12-10')));
        $this->assertFalse($day->isBetween(new \DateTime('2015-12-11')));
        $this->assertFalse($day->isBetween(new \DateTime('1900-12-31')));
    }
}
