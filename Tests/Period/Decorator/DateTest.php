<?php

namespace Harbinger\DateTime\Test\Period\Decorator;

use \Harbinger\DateTime\Period;
use \PHPUnit\Framework\TestCase;

class DateTest extends TestCase
{

    public function assertPreConditions()
    {
        $this->assertTrue(class_exists($class = Period\Decorator\Date::class) , 'Class not found: '.$class);
    }

    public function testCreateAPeriodWithValidArgumentsShouldWork()
    {
        $start = new \DateTime('now');
        $end = new \DateTime('now');

        $period = new Period\DateTime($start , $end);
        $date = new Period\Decorator\Date($period);

        $this->assertInstanceOf(
            $instance = Period::class,
            $date,
            sprintf("Should be instance of %s" , $instance)
        );

        $this->assertInstanceOf(
            $instance = Period\Decorator::class,
            $date ,
            sprintf("Should be instance of %s" , $instance)
        );

        $this->assertInstanceOf(
            $instance = Period\AbstractDecorator::class,
            $date,
            sprintf("Should be instance of %s" , $instance)
        );

        $this->assertTrue($date->isBetween(new \DateTime()));
        $this->assertFalse($date->isBetween(new \DateTime('+1 day')));
    }
}
