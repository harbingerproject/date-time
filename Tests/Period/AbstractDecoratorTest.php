<?php

namespace Harbinger\DateTime\Test\Period;

use \Harbinger\DateTime\Period;
use \PHPUnit\Framework\TestCase;

class AbstractDecoratorTest extends TestCase
{

    private $stub;

    public function assertPreConditions()
    {
        $this->assertTrue(interface_exists($interface = Period::class) , 'Interface not found: '.$interface);
        $this->assertTrue(
            class_exists($class = Period\AbstractDecorator::class),
            'Abstract class not found: '.$class
        );
    }

    public function setUp()
    {
        $start = \DateTime::createFromFormat('H:i:s' , '08:00:00');
        $end = \DateTime::createFromFormat('H:i:s' , '12:00:00');

        $period = new Period\DateTime($start , $end);

        $this->stub = $this->getMockForAbstractClass(Period\AbstractDecorator::class , array($period));
        $this->stub->expects($this->any())
                   ->method('doIsBetween')
                   ->will($this->returnValue(true));

    }

    public function testCreateAPeriodWithValidArgumentsShouldWork()
    {
        $this->assertInstanceOf(
            $instance = Period::class,
            $this->stub ,
            sprintf("Should be instance of %s" , $instance)
        );

        $this->assertInstanceOf(
            $instance = Period\Decorator::class,
            $this->stub ,
            sprintf("Should be instance of %s" , $instance)
        );

        $this->assertInstanceOf(
            $instance = Period\AbstractDecorator::class,
            $this->stub ,
            sprintf("Should be instance of %s" , $instance)
        );

        $this->assertTrue($this->stub->isBetween(new \DateTime()));

        $newStub = $this->getMockForAbstractClass(Period\AbstractDecorator::class , array($this->stub));
        $newStub->expects($this->any())
                ->method('doIsBetween')
                ->will($this->returnValue(false));

        $this->assertFalse($newStub->isBetween(new \DateTime()));
    }
}
