<?php

namespace Harbinger\DateTime\Test\Period;

use Harbinger\DateTime;
use \PHPUnit\Framework\TestCase;

class DateTimeTest extends TestCase
{

    public function assertPreConditions()
    {
        $this->assertTrue(interface_exists($interface = DateTime\Period::class) , 'Interface not found: '.$interface);
        $this->assertTrue(class_exists($class = DateTime\Period\DateTime::class) , 'Class not found: '.$class);
    }

    public function testCreateAPeriodWithValidArgumentsShouldWork()
    {

        $start = \DateTime::createFromFormat('H:i:s' , '08:00:00');
        $end = \DateTime::createFromFormat('H:i:s' , '12:00:00');

        $period = new DateTime\Period\DateTime($start , $end);

        $this->assertInstanceOf(DateTime\Period::class , $period);

        $this->assertSame(
            $start,
            $period->getStart(),
            sprintf('%s should be same %s' , print_r($start , true) , print_r($period->getStart() , true))
        );

        $this->assertSame(
            $end,
            $period->getEnd(),
            sprintf('%s should be same %s' , print_r($end , true) , print_r($period->getEnd() , true))
        );
    }

    /**
     * @expectedException \Harbinger\DateTime\RuntimeException
     **/
    public function testCreateAPeriodWithInvalidArgumentsShouldThrowAnException()
    {
        $start = \DateTime::createFromFormat('H:i:s' , '12:00:00');
        $end = \DateTime::createFromFormat('H:i:s' , '08:00:00');

        $period = new DateTime\Period\DateTime($start , $end);
    }

    public function testGetEstimatedShouldReturnThePositiveDifferenBetweenStartAndEndTime()
    {
        $start = \DateTime::createFromFormat('H:i:s' , '08:00:00');
        $end = \DateTime::createFromFormat('H:i:s' , '12:00:00');

        $period = new DateTime\Period\DateTime($start , $end);

        $this->assertEquals(
            $dateInterval = new \DateInterval('PT4H'),
            $estimated = $period->getEstimated(),
            sprintf('%s should be the same value from %s' , print_r($dateInterval , true) , print_r($estimated , true))
        );
        $this->assertEquals(
            $dateInterval = (new \DateTime('00:00:00'))->diff(new \DateTime('04:00:00')),
            $estimated = $period->getEstimated(),
            sprintf('%s should be the same value from %s' , print_r($dateInterval , true) , print_r($estimated , true))
        );
        $this->assertNotEquals(
            $dateInterval = (new \DateTime('04:00:00'))->diff(new \DateTime('00:00:00')),
            $estimated = $period->getEstimated(),
            sprintf(
                '%s should be the inverse value from %s',
                print_r($dateInterval , true),
                print_r($estimated , true)
            )
        );

        $this->assertEquals(
            $dateInterval = (new \DateTime('04:00:00'))->diff(new \DateTime('00:00:00') , true),
            $estimated = $period->getEstimated(),
            sprintf('%s should be the same value from %s' , print_r($dateInterval , true) , print_r($estimated , true))
        );
    }
}
