<?php

/**
 * This file is part of Harbinger Project.
 *
 * Copyright (c) 2015, Gabriel Heming <gabriel.heming@hotmail.com>.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *
 *   * Neither the name of Gabriel Heming nor the names of his
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * @author Gabriel Heming <gabriel.heming@hotmail.com>
 * @copyright 2015 Gabriel Heming. All rights reserved.
 * @license http://www.opensource.org/licenses/bsd-license.php BSD License
 **/

namespace Harbinger\DateTime;

use Harbinger\Iterator\Filter;

/**
 * Represents a worked time period
 * @package Harbinger
 * @subpackage DateTime
 * @author Gabriel Heming <gabriel.heming@hotmail.com>
 **/
class Expedient
{

    /**
     * @var \Harbinger\DateTime\Period
     **/
    private $period;

    /**
     * @var int[]
     **/
    private $calendarDayCollection;

    /**
     * @param \Harbinger\DateTime\Period $period
     * @param int[] $calendarDayCollection Define by \Harbinger\DateTime\Calendar\Day constants
     **/
    public function __construct(Period $period , Collection\Calendar\Day $calendarDayCollection = null)
    {
        $this->period = $period;

        $this->calendarDayCollection = $calendarDayCollection instanceof Collection\Calendar\Day
                                       ? $calendarDayCollection
                                       : new Collection\Calendar\Day();
    }

    /**
     * @return \Harbinger\DateTime\Collection\Calendar\Day
     **/
    public function getCalendarDayCollection()
    {
        return $this->calendarDayCollection;
    }

    /**
     * @param \Harbinger\DateTime\Collection\Calendar\Day $calendarDay
     * @return $this
     **/
    public function setCalendarDayCollection(Collection\Calendar\Day $calendarDayCollection)
    {
        $this->calendarDayCollection = $calendarDayCollection;
    }

    /**
     * get the start of period
     * @return \DateTime
     * @see http://php.net/manual/en/class.datetime.php DateTime class documentation
     * @see http://php.net/manual/en/book.datetime.php DateTime library documentation
     **/
    public function getPeriod()
    {
        return $this->period;
    }

    /**
     * {@inheritsdoc}
     **/
    public function isExpedientDay(\DateTimeInterface $dateTime)
    {
        $weekDay = (int)$dateTime->format('w');

        $callback = function ($calendarDay) use ($weekDay) {
            return $calendarDay->equals($weekDay);
        };

        foreach (new \CallbackFilterIterator($this->getCalendarDayCollection() , $callback) as $calendarDay) {
            return true;
        }

        return false;
    }

    /**
     * Calculate the worked time for a period
     * @param \Harbinger\DateTime\Period $workedPeriod
     * @return \DateInterval
     * @throws \Harbinger\DateTime\RuntimeException If end date is greater than start date
     **/
    public function calculateWorkedPeriod(Period $period)
    {
        // the days conversion is necessary because DateTime class carry weekdays with himself
        $start = new \DateTime($period->getStart()->format('Y-m-d').' '.$this->period->getStart()->format('H:i:s'));
        $end = new \DateTime($period->getStart()->format('Y-m-d').' '.$this->period->getEnd()->format('H:i:s'));

        $started = $period->getStart() > $start  ? $period->getStart() : $start;
        $ended = $period->getEnd() > $end  ? $end : $period->getEnd();

        if ($started > $ended) {
            throw new RuntimeException('Cannot calculate worked period');
        }

        return $started->diff($ended , true);
    }
}
