<?php

/**
 * This file is part of Harbinger Project.
 *
 * Copyright (c) 2015, Gabriel Heming <gabriel.heming@hotmail.com>.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *
 *   * Neither the name of Gabriel Heming nor the names of his
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * @author Gabriel Heming <gabriel.heming@hotmail.com>
 * @copyright 2015 Gabriel Heming. All rights reserved.
 * @license http://www.opensource.org/licenses/bsd-license.php BSD License
 **/

namespace Harbinger\DateTime\Period;

/**
 * Represents a time period
 * @package Harbinger
 * @subpackage DateTime\Period
 * @author Gabriel Heming <gabriel.heming@hotmail.com>
 **/
abstract class AbstractDecorator implements \Harbinger\DateTime\Period\Decorator
{

    /**
     * @var \Harbinger\DateTime\Period
     **/
    protected $period;

    /**
     * @param \Harbinger\DateTime\Period $period
     **/
    public function __construct(\Harbinger\DateTime\Period $period)
    {
        $this->period = $period;
    }

    /**
     * {@inheritsdoc}
     **/
    public function getStart()
    {
        return $this->period->getStart();
    }

    /**
     * {@inheritsdoc}
     **/
    public function getEnd()
    {
        return $this->period->getEnd();
    }

    /**
     * {@inheritsdoc}
     **/
    public function getEstimated()
    {
        return $this->period->getEstimated();
    }

    /**
     * {@inheritsdoc}
     **/
    public function isEarlierThan(\Harbinger\DateTime\Period $period)
    {
        return $this->period->isEarlierThan($period);
    }

    /**
     * {@inheritsdoc}
     **/
    public function isBetween(\DateTimeInterface $dateTime)
    {
        if ($this->period instanceof AbstractDecorator && !$this->period->isBetween($dateTime)) {
            return false;
        }

        return $this->doIsBetween($dateTime);
    }

    /**
     * @param \DateTimeInterface $dateTime
     * @return boolean
     * @see http://php.net/manual/en/class.datetimeinterface.php Documentation of DateTimeInterface
     * @see http://php.net/manual/en/book.datetime.php DateTime library documentation
     **/
    abstract protected function doIsBetween(\DateTimeInterface $dateTime);
}
