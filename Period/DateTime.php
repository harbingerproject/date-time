<?php

/**
 * This file is part of Harbinger Project.
 *
 * Copyright (c) 2015, Gabriel Heming <gabriel.heming@hotmail.com>.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *
 *   * Neither the name of Gabriel Heming nor the names of his
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * @author Gabriel Heming <gabriel.heming@hotmail.com>
 * @copyright 2015 Gabriel Heming. All rights reserved.
 * @license http://www.opensource.org/licenses/bsd-license.php BSD License
 **/

namespace Harbinger\DateTime\Period;

use Harbinger\DateTime\Period;
use Harbinger\DateTime\RuntimeException;

/**
 * Represents a date/time period
 * @package Harbinger
 * @subpackage DateTime
 * @author Gabriel Heming <gabriel.heming@hotmail.com>
 **/
class DateTime implements Period
{

    /**
     * @var \DateTimeInterface
     * @see http://php.net/manual/en/class.datetimeinterface.php Documentation of DateTimeInterface
     * @see http://php.net/manual/en/book.datetime.php DateTime library documentation
     **/
    private $start;

    /**
     * @var \DateTimeInterface
     * @see http://php.net/manual/en/class.datetimeinterface.php Documentation of DateTimeInterface
     * @see http://php.net/manual/en/book.datetime.php DateTime library documentation
     **/
    private $end;

    /**
     * @param \DateTimeInterface $start
     * @param \DateTimeInterface $end
     * @throws Harbinger\DateTime\RuntimeException If end date is lesse than start date
     * @see http://php.net/manual/en/class.datetimeinterface.php Documentation of DateTimeInterface
     * @see http://php.net/manual/en/book.datetime.php DateTime library documentation
     **/
    public function __construct(\DateTimeInterface $start , \DateTimeInterface $end)
    {
        if ($start > $end) {
            throw new RuntimeException('End time need be equal or grater than start time');
        }

        $this->start = $start;
        $this->end = $end;
    }

    /**
     * {@inheritsdoc}
     **/
    public function getStart()
    {
        return $this->start;
    }

    /**
     * {@inheritsdoc}
     **/
    public function getEnd()
    {
        return $this->end;
    }

    /**
     * {@inheritsdoc}
     **/
    public function getEstimated()
    {
        return $this->getStart()->diff($this->getEnd() , true);
    }

    /**
     * {@inheritsdoc}
     **/
    public function isBetween(\DateTimeInterface $dateTime)
    {
        return $dateTime >= $this->getStart() && $dateTime < $this->getEnd();
    }

    /**
     * {@inheritsdoc}
     **/
    public function isEarlierThan(Period $period)
    {
        return (boolean)($this->getStart() < $period->getStart());
    }
}
